# Copyright (c) 2020 LG Electronics, Inc.
# Copyright (c) 2022 Wind River Systems, Inc.
# Copyright (c) 2019-2024 NXP

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://package.xml;beginline=13;endline=13;md5=f12ef8c0445c08084ae92cf2dcb7ee92"

ROS_BUILD_DEPENDS:remove = "python3-pykdl"
ROS_EXPORT_DEPENDS:remove = "python3-pykdl"
ROS_EXEC_DEPENDS:remove = "python3-pykdl"

ROS_BUILDTOOL_DEPENDS += " \
    ${PYTHON_PN}-pykdl-native \
"