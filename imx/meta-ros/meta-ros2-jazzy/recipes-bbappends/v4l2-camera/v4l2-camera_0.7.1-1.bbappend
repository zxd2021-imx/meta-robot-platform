# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS += " \
    ament-cmake-ros-native \
"

# v4l2-camera/0.7.0-1-r0/recipe-sysroot/usr/include/libstatistics_collector/libstatistics_collector/topic_statistics_collector/received_message_period.hpp:174:32: error: unused parameter 'message_info' [-Werror=unused-parameter]
CXXFLAGS += "-Wno-error=unused-parameter"