# Copyright (c) 2021 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS += "ament-cmake-native"

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-CMakeLists.txt-Add-SYSROOT-in-BACKWARD_ROS_INSTALL_P.patch"

BBCLASSEXTEND = "native nativesdk"
