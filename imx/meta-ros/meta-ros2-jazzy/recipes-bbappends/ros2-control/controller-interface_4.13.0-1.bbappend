# Copyright (c) 2022 Wind River Systems, Inc.
# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS = " \
    ament-cmake-ros-native \
"
FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-CMakeLists.txt-Remove-compile-options-Werror-shadow-.patch"