# Copyright (c) 2019-2024 NXP

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-CMakeLists.txt-Remove-compile-options-Werror-shadow-.patch"