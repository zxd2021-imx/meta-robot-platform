# Copyright (c) 2019-2024 NXP

# All rviz recipes aren't available without qtbase
ROS_EXEC_DEPENDS:remove = "fuse-viz"
