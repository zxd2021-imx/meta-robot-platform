# Copyright (c) 2021 LG Electronics, Inc.

# Doesn't need runtime dependency on ceres-solver
ROS_EXEC_DEPENDS:remove = "ceres-solver"

# tmp/work/armv8a-poky-linux/fuse-optimizers/1.1.1-1/recipe-sysroot/opt/ros/jazzy/include/rcutils/rcutils/logging_macros.h:81:27: error: format not a string literal and no format arguments [-Werror=format-security]
CXXFLAGS += "-Wno-error=format-security"