# Copyright (c) 2023 Wind River Systems, Inc.
# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS += " \
    rosidl-default-generators-native \
    generate-parameter-library-py-native \
"

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-CMakeLists.txt-Remove-compile-options-Werror-shadow-.patch"