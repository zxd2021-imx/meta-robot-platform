# Copyright (c) 2021 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS += " \
    generate-parameter-library-py-native \
"

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-CMakeLists.txt-Remove-compile-options-Werror-shadow.patch"

# tmp/work/armv8a-poky-linux/joint-trajectory-controller/4.11.0-1/recipe-sysroot/opt/ros/jazzy/include/rcutils/rcutils/logging_macros.h:81:27: error: format not a string literal and no format arguments [-Werror=format-security]
CXXFLAGS += "-Wno-error=format-security"