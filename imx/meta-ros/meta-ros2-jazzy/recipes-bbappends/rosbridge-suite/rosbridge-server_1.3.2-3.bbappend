# Copyright (c) 2021 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP
#
ROS_BUILDTOOL_DEPENDS += " \
    ament-cmake-gmock \
    ament-cmake-gtest \
    ament-cmake-pytest \
    ament-cmake-ros \
"

# Doesn't need runtime dependency on ceres-solver
ROS_EXEC_DEPENDS:remove = " \
    python3-twisted \
"