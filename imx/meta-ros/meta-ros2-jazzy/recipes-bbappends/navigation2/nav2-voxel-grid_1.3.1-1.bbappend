# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS = " \
    ament-cmake-ros-native \
    rosidl-core-generators-native \
"

# tmp/work/armv8a-poky-linux/nav2-voxel-grid/1.3.1-1/recipe-sysroot/opt/ros/jazzy/include/rclcpp/rclcpp/create_subscription.hpp:101:14: error: declaration of 'subscription_topic_stats' shadows a previous local [-Werror=shadow]
CXXFLAGS += "-Wno-error=shadow"