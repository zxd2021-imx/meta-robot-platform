# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS = " \
    ament-cmake-ros-native \
    rosidl-core-generators-native \
"