# Copyright (c) 2019-2024 NXP

# tmp/work/armv8a-poky-linux/nav2-amcl/1.3.1-1/recipe-sysroot/opt/ros/jazzy/include/rclcpp/rclcpp/exceptions/exceptions.hpp:71:79: error: declaration of 'invalid_index' shadows a member of 'rclcpp::exceptions::InvalidNodeNameError' [-Werror=shadow]
CXXFLAGS += "-Wno-error=shadow"

# tmp/work/armv8a-poky-linux/nav2-amcl/1.3.1-1/recipe-sysroot/opt/ros/jazzy/include/class_loader/class_loader/meta_object.hpp:57:27: error: 'class class_loader::impl::AbstractMetaObjectBase' has virtual functions and accessible non-virtual destructor [-Werror=non-virtual-dtor]
CXXFLAGS += "-Wno-error=non-virtual-dtor"

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-Add-scan_frame_id-to-match-output-frame-id-of-depthi.patch"