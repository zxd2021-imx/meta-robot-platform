# Copyright (c) 2023 Wind River Systems, Inc.
# Copyright (c) 2019-2024 NXP

LICENSE = "BSD-3-Clause"

DEPENDS += "sip3-native python3-pyqt5-native"
