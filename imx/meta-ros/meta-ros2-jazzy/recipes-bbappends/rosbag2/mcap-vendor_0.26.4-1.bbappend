# Copyright (c) 2022 Wind River Systems, Inc.
# Copyright (c) 2019-2024 NXP

# Use Bitbake to fetch https://github.com/foxglove/mcap.git
FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
#SRC_URI += "file://0001-CMakeLists.txt-fetch-uncrustify-with-bitbake-fetcher.patch"
SRC_URI += " \
    git://github.com/foxglove/mcap.git;protocol=https;name=mcap;destsuffix=git/mcap;branch=main;lfs=0 \
    file://0001-CMakeLists.txt-fetch-dependencies-with-bitbake-fetch.patch\
"

# releases/cpp/v1.3.0
SRCREV_mcap = "f87352f0aaae81628bdd7ffc43f7563f6bd98547"

SRCREV_FORMAT += "_mcap"

#
# PN package in zstd-vendor is empty and not created, remove runtime dependency on it
ROS_EXEC_DEPENDS:remove = "zstd-vendor"

inherit pkgconfig