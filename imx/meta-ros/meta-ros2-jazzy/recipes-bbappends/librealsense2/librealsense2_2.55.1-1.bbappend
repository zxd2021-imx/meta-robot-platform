# Copyright (c) 2023 Wind River Systems, Inc.
# Copyright (c) 2019-2024 NXP

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-CmakeLists.txt-use-nlohmann-json-in-Yocto-instead-of.patch"

ROS_BUILD_DEPENDS:remove = " \
    mesa \
"

ROS_EXPORT_DEPENDS:remove = " \
    mesa \
"

ROS_EXEC_DEPENDS:remove = " \
    mesa \
"

ROS_BUILD_DEPENDS += " \
    mesa-gl \
    nlohmann-json \
    libglu \
"

inherit ros_insane_dev_so

EXTRA_OECMAKE += " -DFORCE_LIBUVC=ON -DCHECK_FOR_UPDATES=OFF"

do_install:append() {
    rm ${D}/home -rf
}
