# Copyright (c) 2021 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP

ROS_BUILD_DEPENDS += " \
    octomap \
"

# ERROR: geometric-shapes-2.2.1-1-r0 do_package_qa: QA Issue:
# non -dev/-dbg/nativesdk- package geometric-shapes contains symlink .so '/opt/ros/jazzy/lib/libgeometric_shapes.so' [dev-so]
inherit ros_insane_dev_so