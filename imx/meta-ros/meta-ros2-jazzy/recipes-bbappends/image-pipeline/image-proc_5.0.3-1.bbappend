# Copyright (c) 2020 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS += " \
    eigen3-cmake-module \
"

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-Fix-the-compile-issues-which-assign-instances-of-a-c.patch"