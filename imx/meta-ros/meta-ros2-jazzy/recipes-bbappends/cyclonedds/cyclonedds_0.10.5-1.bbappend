# Copyright (c) 2019-2021 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP

# IDLC depends on maven and we don't want meta-ros to depend on meta-java just for that
EXTRA_OECMAKE += "-DBUILD_IDLC=OFF -DBUILD_DDSPERF=NO"
EXTRA_OECMAKE += "-DENABLE_SSL=YES -DENABLE_SHM=YES -DENABLE_TYPE_DISCOVERY=YES -DENABLE_TOPIC_DISCOVERY=YES"

FILES:${PN}-dev += "${datadir}/CycloneDDS"

inherit ros_insane_dev_so

DEPENDS += "cyclonedds-native"

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-confgen-install-confgen-to-be-used-in-target-cyclone.patch"
LICENSE = "EPL-2.0 & EDL-1.0"
