# Copyright (c) 2019-2021 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += " \
    file://0001-CmakeLists.txt-set-project-Library-to-shared.patch \
    file://0001-Fix-compile-error-which-use-mutex-of-absl-lib.patch \
"

DEPENDS += "eigen3-cmake-module"
