# Copyright (c) 2019-2024 NXP

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-Work-around-the-compile-issues-in-Yocto-scarthgap-wh.patch"