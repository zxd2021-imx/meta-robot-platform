#
# Copyright (c) 2013 Stefan Herbrechtsmeier, Bielefeld University
# Copyright (c) 2019-2020 LG Electronics, Inc.
# Copyright (c) Qualcomm Innovation Center, Inc. All rights reserved
# Copyright (c) 2019-2024 NXP
#

ros_prefix ?= "${base_prefix}/opt/ros/${ROS_DISTRO}"

ros_bindir = "${ros_prefix}/bin"
ros_sbindir = "${ros_prefix}/sbin"
ros_libdir = "${ros_prefix}/${baselib}"
ros_libexecdir = "${ros_libdir}/${ROS_BPN}"
ros_includedir = "${ros_prefix}/include"
ros_datadir = "${ros_prefix}/share"
ros_sysconfdir = "${ros_prefix}/etc"
ros_stacksdir = "${ros_prefix}/stacks"
ros_toolsdir = "${ros_prefix}/tools"

# Used by chrpath.bbclass
PREPROCESS_RELOCATE_DIRS += " \
    ${ros_bindir} \
    ${ros_libdir} \
"

# ROS_PYTHON_VERSION is usually set in generated/superflore-ros-distro.inc, but
# in case superflore-ros-distro.inc isn't included default to 3
ROS_PYTHON_VERSION ?= "3"
inherit ${@'python3-dir' if d.getVar('ROS_PYTHON_VERSION') == '3' else 'python-dir'}

PKG_CONFIG_PATH .= ":${PKG_CONFIG_DIR}:${STAGING_DIR_HOST}${ros_libdir}/pkgconfig:${STAGING_DATADIR}/pkgconfig"
PYTHON_SITEPACKAGES_DIR = "${ros_libdir}/${PYTHON_DIR}/site-packages"
export PYTHONPATH = "${STAGING_DIR_NATIVE}${PYTHON_SITEPACKAGES_DIR}"
PYTHONPATH:class-native = "${PYTHON_SITEPACKAGES_DIR}"

FILES:${PN} += "\
    ${ros_bindir} ${ros_libexecdir} ${ros_libdir}/lib*.so \
    ${PYTHON_SITEPACKAGES_DIR} \
    ${ros_datadir} \
    ${ros_sysconfdir} \
    ${ros_stacksdir} \
    ${ros_toolsdir} \
    ${ros_prefix} \
    "

FILES:${PN}-dev += "\
    ${ros_includedir} \
    ${ros_libdir}/pkgconfig \
    ${PYTHON_SITEPACKAGES_DIR}/*.la \
    ${ros_datadir}/${ROS_BPN}/cmake \
    ${datadir}/${ROS_BPN}/cmake \
    ${datadir}/${ROS_BPN}/*.template \
    "

FILES:${PN}-dbg += "\
    ${ros_bindir}/.debug ${ros_libexecdir}/.debug ${ros_libdir}/.debug \
    ${ros_datadir}/*/bin/.debug \
    ${PYTHON_SITEPACKAGES_DIR}/.debug \
    ${PYTHON_SITEPACKAGES_DIR}/*/.debug \
    ${PYTHON_SITEPACKAGES_DIR}/*/*/.debug \
    "

FILES:${PN}-staticdev += "\
    ${ros_libdir}/*.a \
    ${ros_libdir}/${BPN}/*.a \
    "

SYSROOT_DIRS:append = " \
    ${ros_includedir} \
    ${ros_libdir} \
    ${ros_datadir} \
    ${ros_stacksdir} \
    ${ros_sysconfdir} \
    ${ros_bindir} \
    ${ros_sbindir} \
    "

SYSROOT_DIRS_NATIVE:append = " \
    ${ros_bindir} \
    ${ros_libexecdir} \
    "

INSANE_SKIP:${PN} += "libdir buildpaths"
INSANE_SKIP:${PN}-dbg += "libdir buildpaths"
INSANE_SKIP:${PN}-dev += " buildpaths"
INSANE_SKIP:${PN}-staticdev += " buildpaths"
INSANE_SKIP:${PN}-src += " buildpaths"