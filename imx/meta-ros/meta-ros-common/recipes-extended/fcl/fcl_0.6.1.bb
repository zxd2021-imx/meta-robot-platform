#
# Copyright Open Source Robotics Foundation

inherit ros_distro_${ROS_DISTRO}
inherit ros_superflore_generated

DESCRIPTION = "FCL is a library for performing three types of proximity queries on a pair of geometric models composed of triangles and octrees."
HOMEPAGE = "https://github.com/flexible-collision-library/fcl"
SECTION = "devel"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=7de20b14c33be61ee0c714b41e286d0b"

ROS_CN = "fcl"
ROS_BPN = "fcl"

ROS_BUILD_DEPENDS = " \
    boost \
    libccd \
    libeigen \
"

ROS_BUILDTOOL_DEPENDS = " \
    cmake-native \
"

ROS_EXPORT_DEPENDS = " \
    boost \
    libccd \
    libeigen \
"
ROS_BUILDTOOL_EXPORT_DEPENDS = ""

ROS_EXEC_DEPENDS = ""

# Currently informational only -- see http://www.ros.org/reps/rep-0149.html#dependency-tags.
ROS_TEST_DEPENDS = ""

DEPENDS = "${ROS_BUILD_DEPENDS} ${ROS_BUILDTOOL_DEPENDS}"
# Bitbake doesn't support the "export" concept, so build them as if we needed them to build this package (even though we actually
# don't) so that they're guaranteed to have been staged should this package appear in another's DEPENDS.
DEPENDS += "${ROS_EXPORT_DEPENDS} ${ROS_BUILDTOOL_EXPORT_DEPENDS}"

RDEPENDS:${PN} += "${ROS_EXEC_DEPENDS}"

SRCREV = "97455a46de121fb7c0f749e21a58b1b54cd2c6be"
ROS_BRANCH ?= "branch=master"
SRC_URI = " \
    git://github.com/flexible-collision-library/fcl;${ROS_BRANCH};protocol=https \
"

S = "${WORKDIR}/git" 

ROS_BUILD_TYPE = "cmake"

inherit ros_${ROS_BUILD_TYPE} pkgconfig ros_insane_dev_so

EXTRA_OECMAKE += "-DCMAKE_BUILD_TYPE=Release -DFCL_NO_DEFAULT_RPATH=OFF ${EXTRA_OECMAKE_SSE}"
EXTRA_OECMAKE_SSE = "-DFCL_USE_X64_SSE=OFF"
EXTRA_OECMAKE_SSE:x86-64 = ""