# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS += " \
    ${PYTHON_PN}-numpy-native \
"

ROS_EXEC_DEPENDS:remove = " \
    slam-toolbox \
"
