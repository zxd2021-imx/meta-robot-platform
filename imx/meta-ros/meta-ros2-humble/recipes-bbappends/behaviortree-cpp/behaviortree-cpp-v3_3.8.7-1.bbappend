# Copyright (c) 2019-2024 NXP

ROS_BUILD_DEPENDS += " \
    ament-cmake-gtest \
    cppzmq \
    gtest \
"

FILES:${PN}-dev += "${libdir}/BehaviorTreeV3/cmake"
