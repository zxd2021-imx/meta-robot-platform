# Copyright (c) 2021 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-PATCH-Add-in-missing-cstdint-include.patch"

# ERROR: rcdiscover-1.0.4-1-r0 do_package_qa: QA Issue: non -dev/-dbg/nativesdk- package contains symlink .so: rcdiscover path '/work/core2-64-oe-linux/rcdiscover/1.0.4-1-r0/packages-split/rcdiscover/usr/lib/librcdiscover.so' [dev-so]
inherit ros_insane_dev_so
