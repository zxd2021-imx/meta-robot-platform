# Copyright (c) 2022 Wind River Systems, Inc.
# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS = " \
    rosidl-adapter-native \
    rosidl-generator-py-native \
"

ROS_EXEC_DEPENDS:remove = " \
    graphviz \
"
