# Copyright (c)2019-2024 NXP

LICENSE = "BSD-3-Clause"

ROS_BUILDTOOL_DEPENDS += " \
    ${PYTHON_PN}-numpy-native \
"

inherit pkgconfig
