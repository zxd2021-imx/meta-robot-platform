# Copyright (c) 2020 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS += " \
    ${PYTHON_PN}-numpy-native \
    generate-parameter-library-py-native \
"

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += " \
    file://0001-filter_base.hpp-use-unsigned-long-instead-of-size_t-.patch \
"
