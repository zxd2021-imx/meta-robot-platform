# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS += " \
    ${PYTHON_PN}-numpy-native \
    generate-parameter-library-py-native \
"
