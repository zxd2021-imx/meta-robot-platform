# Copyright (c) 2021 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP

inherit python3targetconfig

ROS_BUILDTOOL_DEPENDS += " \
    python3-numpy-native \
    rosidl-adapter \
    rosidl-generator-py-native \
"

INSANE_SKIP:${PN} += "already-stripped"
