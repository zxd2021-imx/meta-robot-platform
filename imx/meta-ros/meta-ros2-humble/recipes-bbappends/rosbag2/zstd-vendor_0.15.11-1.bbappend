# Copyright (c) 2020 LG Electronics, Inc.
# Copyright (c) 2023 Wind River Systems, Inc.

inherit pkgconfig

LICENSE = "Apache-2.0"
