# Copyright (c) 2019-2024 NXP

ROS_BUILD_DEPENDS += " \
    rclcpp \
    sensor-msgs \
    pluginlib \
"

ROS_BUILDTOOL_DEPENDS += " \
    ${PYTHON_PN}-numpy-native \
"

CXXFLAGS += " -Wno-error=format-security"
