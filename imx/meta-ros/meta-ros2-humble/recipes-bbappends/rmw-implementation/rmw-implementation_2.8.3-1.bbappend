# Copyright (c) 2020-2021 LG Electronics, Inc.
# Copyright 2019-2024 NXP

#Chose ROS middleware implementation at build time (rmw-fastrtps-cpp or rmw-cyclonedds-cpp)
EXTRA_OECMAKE += "\
  -DRMW_IMPLEMENTATION=rmw_cyclonedds_cpp \
"

ROS_BUILD_DEPENDS:remove = "${@bb.utils.contains('ROS_WORLD_SKIP_GROUPS', 'connext', 'rmw-connext-cpp rmw-connextdds', '', d)}"

ROS_BUILDTOOL_DEPENDS:append = " \
    rosidl-adapter-native \
    rosidl-generator-cpp-native \
"

ROS_EXEC_DEPENDS:append = " \
    rmw-fastrtps-cpp \
    rmw-cyclonedds-cpp \
"
