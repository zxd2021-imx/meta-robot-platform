# Copyright (c) 2019-2024 NXP

ROS_BUILD_DEPENDS += "${PYTHON_PN}-pyopenssl"
ROS_EXEC_DEPENDS += "${PYTHON_PN}-pyopenssl"

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"
SRC_URI += "file://0001-Fix-incompatibility-issue-with-python3-cryptography-.patch"
