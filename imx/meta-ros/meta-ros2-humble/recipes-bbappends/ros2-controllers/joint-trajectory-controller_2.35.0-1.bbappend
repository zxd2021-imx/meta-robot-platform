# Copyright (c) 2021 LG Electronics, Inc.
# Copyright (c) 2019-2024 NXP

ROS_BUILDTOOL_DEPENDS += " \
    ament-cmake-ros-native \
    python3-numpy-native \
    rosidl-adapter-native \
    rosidl-generator-c-native \
    rosidl-generator-cpp-native \
    rosidl-generator-py-native \
    rosidl-typesupport-cpp-native \
    rosidl-typesupport-fastrtps-c-native \
    rosidl-typesupport-fastrtps-cpp-native \
    rosidl-typesupport-introspection-cpp-native \
"

ROS_BUILDTOOL_DEPENDS += " \
    generate-parameter-library-py-native \
"

# joint-trajectory-controller/2.35.0-1/recipe-sysroot/opt/ros/humble/include/rcutils/rcutils/logging_macros.h:79:18: error: format not a string literal and no format arguments [-Werror=format-security]
CXXFLAGS += "-Wno-error=format-security"
