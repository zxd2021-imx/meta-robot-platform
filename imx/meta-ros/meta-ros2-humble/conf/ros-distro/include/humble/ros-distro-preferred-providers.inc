# humble/ros-distro-preferred-providers.inc
#
# Copyright (c) 2019-2024 NXP

# Set PREFERRED_PROVIDER_<PN> here for non-platform packages and to override those set in
# ros-distro-platform-preferred-providers.inc .
