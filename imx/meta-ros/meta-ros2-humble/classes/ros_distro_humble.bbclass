# Every ROS recipe, generated or not, must contain "inherit ros_distro_${ROS_DISTRO}".
#
# Copyright (c) 2019-2024 NXP

ROS_DISTRO = "humble"

inherit ${ROS_DISTRO_TYPE}_distro
