hostname:pn-base-files ?= "imx-robot"

# Use i.MX real time Kernel providers
#XENOMAI_KERNEL_MODE ?= "cobalt"
#XENOMAI_KERNEL_MODE ?= "mercury"
PREFERRED_VERSION_openblas ?= "0.3.17"
#PREFERRED_VERSION_cryptodev-linux ?= "1.12.imx"
#PREFERRED_VERSION_cryptodev-module ?= "1.12.imx"
#PREFERRED_VERSION_cryptodev-tests ?= "1.12.imx"
PREFERRED_VERSION_libg2o ?= "20201223"
PREFERRED_VERSION_ceres-solver ?= "2.1.0"