#
# Copyright Open Source Robotics Foundation

inherit ros_distro_${ROS_DISTRO}
inherit ros_superflore_generated

DESCRIPTION = "A generic canopen implementation for ROS"
AUTHOR = "Denis Stogl <denis@stoglrobotics.de>"
HOMEPAGE = "https://ros-industrial.github.io/ros2_canopen"
SECTION = "devel"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://package.xml;beginline=10;endline=10;md5=82f0323c08605e5b6f343b05213cf7cc"

ROS_CN = "ros2_canopen"
ROS_BPN = "canopen_ros2_controllers"

ROS_BUILD_DEPENDS = " \
    rclcpp \
    rclcpp-lifecycle \  
    controller-interface \
    controller-manager \
    hardware-interface \
    pluginlib \    
    realtime-tools \
    std-msgs \
    std-srvs \
    canopen-402-driver \
    canopen-interfaces \
    canopen-proxy-driver \
"

ROS_BUILDTOOL_DEPENDS = " \
    ${@bb.utils.contains('ROS_DISTRO_TYPE', 'ros1', 'catkin-native', '', d)} \
    ${@bb.utils.contains('ROS_DISTRO_TYPE', 'ros2', 'ament-cmake-native', '', d)} \
"

ROS_EXPORT_DEPENDS = " \
    rclcpp \
    rclcpp-lifecycle \  
    controller-interface \
    controller-manager \
    hardware-interface \
    pluginlib \    
    realtime-tools \
    std-msgs \
    std-srvs \
    canopen-402-driver \
    canopen-interfaces \
    canopen-proxy-driver \
"

ROS_BUILDTOOL_EXPORT_DEPENDS = ""

ROS_EXEC_DEPENDS = " \
    rclcpp \
    rclcpp-lifecycle \  
    controller-interface \
    controller-manager \
    hardware-interface \
    pluginlib \    
    realtime-tools \
    std-msgs \
    std-srvs \
    canopen-402-driver \
    canopen-interfaces \
    canopen-proxy-driver \
"

# Currently informational only -- see http://www.ros.org/reps/rep-0149.html#dependency-tags.
ROS_TEST_DEPENDS = ""

DEPENDS = "${ROS_BUILD_DEPENDS} ${ROS_BUILDTOOL_DEPENDS}"
# Bitbake doesn't support the "export" concept, so build them as if we needed them to build this package (even though we actually
# don't) so that they're guaranteed to have been staged should this package appear in another's DEPENDS.
DEPENDS += "${ROS_EXPORT_DEPENDS} ${ROS_BUILDTOOL_EXPORT_DEPENDS}"

RDEPENDS:${PN} += "${ROS_EXEC_DEPENDS}"

SRC_URI = "git://github.com/ros-industrial/ros2_canopen.git;protocol=https;branch=master"
SRCREV = "9d8317c56b4a048b3414bbbaf03c40b43f75138a"

S = "${WORKDIR}/git/${ROS_BPN}" 

ROS_BUILD_TYPE = "${@bb.utils.contains('ROS_DISTRO_TYPE','ros1','catkin','ament_cmake',d)}"

inherit ros_${ROS_BUILD_TYPE} pkgconfig

CXXFLAGS += "-Wno-error=format-security"