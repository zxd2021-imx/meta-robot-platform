SUMMARY = "Lely core libraries"
DESCRIPTION = "The Lely core libraries are a collection of C and C++ libraries and tools, \
providing hih-performance I/O and sensor/actuator control for robotics and IoT \
applications. The libraries are cross-platform and have few dependencies. They \
can be even be used on bare-metal microcontrollers with as little as 32 kB RAM."
HOMEPAGE = "https://opensource.lely.com/canopen/"
SECTION = "libs"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

DEPENDS += "  ${PYTHON_PN}-empy"

SRC_URI = "git://gitlab.com/lely_industries/lely-core.git;branch=master;protocol=https"
SRCREV = "b63a0b6f79d3ea91dc221724b42dae49894449fc"

SRC_URI += "file://0001-Fix-dcf-tools.patch"

S = "${WORKDIR}/git"

inherit setuptools3 autotools-brokensep pkgconfig

EXTRA_OECONF += "--disable-cython --disable-doc --disable-tests --disable-static --disable-diag"

do_install:append() {
    sed -i 's@^#!/.*python3@#!/usr/bin/env python3@g' ${D}${bindir}/dcfchk
    sed -i 's@^#!/.*python3@#!/usr/bin/env python3@g' ${D}${bindir}/dcf2dev
    sed -i 's@^#!/.*python3@#!/usr/bin/env python3@g' ${D}${bindir}/dcfgen
}

INSANE_SKIP:${PN} += " buildpaths file-rdeps"
BBCLASSEXTEND = "native"