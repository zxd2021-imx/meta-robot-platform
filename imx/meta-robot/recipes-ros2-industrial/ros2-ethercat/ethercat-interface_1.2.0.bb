#
# Copyright Open Source Robotics Foundation

inherit ros_distro_${ROS_DISTRO}
inherit ros_superflore_generated

DESCRIPTION = "Implementation of a Hardware Interface for simple Ethercat module integration with ros2_control and building upon IgH EtherCAT Master for Linux."
AUTHOR = "Maciej Bednarczyk <m.bednarczyk@unistra.fr>"
HOMEPAGE = "https://icube-robotics.github.io/ethercat_driver_ros2"
SECTION = "devel"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://package.xml;beginline=8;endline=8;md5=82f0323c08605e5b6f343b05213cf7cc"

ROS_CN = "ros2_ethercat"
ROS_BPN = "ethercat_interface"

ROS_BUILD_DEPENDS = " \
    rclcpp \
    pluginlib \
    yaml-cpp-vendor \
    ethercat-userspace \
"

ROS_BUILDTOOL_DEPENDS = " \
    ${@bb.utils.contains('ROS_DISTRO_TYPE', 'ros1', 'catkin-native', '', d)} \
    ${@bb.utils.contains('ROS_DISTRO_TYPE', 'ros2', 'ament-cmake-native ament-cmake-ros-native', '', d)} \
"

ROS_EXPORT_DEPENDS = " \
    rclcpp \
    pluginlib \
"

ROS_BUILDTOOL_EXPORT_DEPENDS = ""

ROS_EXEC_DEPENDS = " \
    rclcpp \
    pluginlib \
    ethercat-userspace \
"

# Currently informational only -- see http://www.ros.org/reps/rep-0149.html#dependency-tags.
ROS_TEST_DEPENDS = " \
    ament-lint-auto \
    ament-lint-common \
"

DEPENDS = "${ROS_BUILD_DEPENDS} ${ROS_BUILDTOOL_DEPENDS}"
# Bitbake doesn't support the "export" concept, so build them as if we needed them to build this package (even though we actually
# don't) so that they're guaranteed to have been staged should this package appear in another's DEPENDS.
DEPENDS += "${ROS_EXPORT_DEPENDS} ${ROS_BUILDTOOL_EXPORT_DEPENDS}"

RDEPENDS:${PN} += "${ROS_EXEC_DEPENDS}"

SRC_URI = "git://github.com/ICube-Robotics/ethercat_driver_ros2.git;protocol=https;branch=main"
SRCREV = "f7b63fe2bd8d1a88493d193ad3288d7662df1a5e"
SRC_URI += "file://0001-CMakeLists.txt-Add-support-user-space-Master-of-IgH-.patch;patchdir=../"

S = "${WORKDIR}/git/${ROS_BPN}" 

ROS_BUILD_TYPE = "${@bb.utils.contains('ROS_DISTRO_TYPE','ros1','catkin','ament_cmake',d)}"

inherit ros_${ROS_BUILD_TYPE} pkgconfig

EXTRA_OECMAKE += " -DUSING_ETHERCAT_USERSPACE=ON"