DESCRIPTION = "i.MX Machine Vison for vSLAM." 
SUMMARY = "imx-mv : Machine Vison library" 
SECTION = "libs" 
LICENSE = "Proprietary" 
LIC_FILES_CHKSUM = "file://LICENSE;md5=ca53281cc0caa7e320d4945a896fb837"

SRC_URI = " file://imx-mv.tar.gz" 
SRC_URI[sha256sum] = "2b575ea705b581507fd5cdf70154ca02017ca2926b03010b1b15648832cf4a53"

DEPENDS = "opencv opencl-clhpp tensorflow-lite"
RDEPENDS:${PN} += "tensorflow-lite"

IMX_MV_LIB ??= "libimx-mv.so"
IMX_MV_LIB:mx95-nxp-bsp = "libimx-mv_mali.so"

S = "${WORKDIR}/imx-mv" 

do_install () {
		install -d ${D}${libdir}
		cp ${S}/lib/${IMX_MV_LIB} ${D}${libdir}/libimx-mv.so
		cp ${S}/lib/*.cl ${D}${libdir}
		install -d ${D}${includedir}
		cp ${S}/include/*.h ${D}${includedir}
}

FILES:${PN} += "\
        ${libdir}/* \
        ${bindir}/* \
"
FILES:${PN}-dev = "\
        ${includedir}/* \
"

INSANE_SKIP:${PN} += " rpaths file-rdeps already-stripped"
