DESCRIPTION = "A Robot image with more full-featured + ROS  SDK + OpenCV Linux system functionality installed."

LICENSE = "MIT"

require recipes-fsl/images/imx-image-multimedia.bb

inherit populate_sdk_qt6
inherit ros_distro_${ROS_DISTRO}
inherit ${ROS_DISTRO_TYPE}_image

CONFLICT_DISTRO_FEATURES = "directfb"

IMAGE_INSTALL += " \
    curl \
    packagegroup-imx-ml \
    packagegroup-qt6-imx \
    tzdata \
    ${IMAGE_INSTALL_OPENCV} \
    ${IMAGE_INSTALL_PARSEC} \
    ${IMAGE_INSTALL_PKCS11TOOL} \
"

IMAGE_INSTALL_OPENCV              = ""
IMAGE_INSTALL_OPENCV:imxgpu       = "${IMAGE_INSTALL_OPENCV_PKGS}"
IMAGE_INSTALL_OPENCV:mx93-nxp-bsp = "${IMAGE_INSTALL_OPENCV_PKGS}"
IMAGE_INSTALL_OPENCV_PKGS = " \
    opencv-apps \
    opencv-samples \
    python3-opencv  \
"

IMAGE_INSTALL_PARSEC = " \
    packagegroup-security-tpm2 \
    packagegroup-security-parsec \
    swtpm \
    softhsm \
    os-release \
    ${@bb.utils.contains('MACHINE_FEATURES', 'optee', 'optee-client optee-os', '', d)}  \
"

IMAGE_INSTALL += " cmake git libeigen opencv opencv-dev ceres libg2o dbow2 dbow3 iperf3"

IMAGE_INSTALL += " \
    imx-vslam-ros2-demo imx-aibot2 \
    ${@bb.utils.contains('ROS_DISTRO', 'humble', 'packagegroup-imx-ros-sdk-humble', '', d)} \
    ${@bb.utils.contains('ROS_DISTRO', 'jazzy', 'packagegroup-imx-ros-sdk-jazzy', '', d)} \
"

IMAGE_INSTALL_PKCS11TOOL = ""
IMAGE_INSTALL_PKCS11TOOL:mx8-nxp-bsp = "opensc pkcs11-provider"
IMAGE_INSTALL_PKCS11TOOL:mx9-nxp-bsp = "opensc pkcs11-provider"