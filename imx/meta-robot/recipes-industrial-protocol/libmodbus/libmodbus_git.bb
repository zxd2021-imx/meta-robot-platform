SUMMARY = "A Modbus library"
DESCRIPTION = "libmodbus is a C library designed to provide a fast and robust \
implementation of the Modbus protocol. It runs on Linux, Mac OS X, FreeBSD, \
QNX and Windows."
HOMEPAGE = "http://www.libmodbus.org/"
SECTION = "libs"

LICENSE = "LGPL-2.1-or-later"
LIC_FILES_CHKSUM = "file://COPYING.LESSER;md5=4fbd65380cdd255951079008b364516c"

DEPENDS += " ${@bb.utils.contains('XENOMAI_KERNEL_MODE', 'cobalt', 'xenomai', ' ', d)}"

SRC_URI = "git://github.com/stephane/libmodbus.git;branch=master;protocol=https"
SRCREV = "2cbafa3113e276c3697d297f68e88d112b53174d"

SRC_URI += "file://0001-Enable-libmodbus-to-support-Xenomai.patch"

S = "${WORKDIR}/git"

inherit autotools-brokensep pkgconfig

EXTRA_OECONF += "${@bb.utils.contains('XENOMAI_KERNEL_MODE', 'cobalt', '--with-xenomai-dir=${WORKDIR}/recipe-sysroot/usr', ' ', d)}"

# this file has been created one minute after the configure file, so it doesn't get recreated during configure step
do_configure:prepend() {
	rm -rf ${S}/tests/unit-test.h
}

do_install:append() {
    install -d "${D}/${bindir}"
    cp ${S}/tests/.libs/unit-test-client ${S}/tests/.libs/unit-test-server ${D}/${bindir}
    cp ${S}/tests/.libs/bandwidth-server-one ${S}/tests/.libs/bandwidth-server-many-up ${S}/tests/.libs/bandwidth-client ${D}/${bindir}
}